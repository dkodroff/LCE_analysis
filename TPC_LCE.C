
#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "THStack.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH3F.h"
#include "TProfile.h"
#include "TProfile3D.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMinuit.h"
#include "TColor.h"
#include "TLine.h"
#include "TLatex.h"
#include "TSystem.h"


#include "/global/homes/d/dsk192/analysis_2019/libBacc/BaccRootConverterEvent.h"

using namespace std;

void TPC_LCE(){


  //----------------------------------------------------------                                                                                                            
  //----------------------------------------------------------                                                                                                        

  string fileName = "/global/cfs/cdirs/lz/users/dkodroff/TPCPhotonSims/Data/pbomb_tpc_v5.3.8_MDC3_1mil_test696336367.root";

  //---------------------------------------------------------                                                                                                               
  // Open the root file and get the data tree                                                                                                                             
  //----------------------------------------------------------                                                                                                              
  
  TFile* f = TFile::Open(fileName.c_str());
  TTree* DataTree = (TTree*)f->Get("DataTree");
  //----------------------------------------------------------                                                                                                          
  // Create the event structure object and link it to the                                                                                                             
  // Data tree                                                                                                                                                          
  //----------------------------------------------------------                                                                                                                                              
  BaccRootConverterEvent* evt = new BaccRootConverterEvent;
  DataTree->SetBranchAddress("Event",&evt);
  //----------------------------------------------------------                                                                                                              
  // Get the #  of entries                                                                                                                                              
  //----------------------------------------------------------                                                                                                                                             
  int nPrimaries = 1e7;
  int nEntries = DataTree->GetEntries();

  // 1) barrel or side skin -> -14.77 < z_cm <= 146.099 && R_cm > 74.3                                                                                                             // 2) PMT dome -> -14.77 < z_cm <= -30                                                                                                                                           // 3) bottom dome -> -30 < z_cm < -44.75 

  //TPC bound 0-145.6 for LiquidXenontTarget
  // RFR bound 0 - -13.7 cm for ReverseFieldRegion
  // what is gas region

  double zlowbound = -20., zhighbound = 160.;
  double xylowbound = -74., xyhighbound = 74.;
  int nBinsZ = 180; // 1cm resolution
  int nBinsxy = 74; // 2cm resolution (1cm in R,Z)

  TH3F *pDet_3D = new TH3F("pDet","",nBinsxy,xylowbound,xyhighbound,nBinsxy,xylowbound,xyhighbound,nBinsZ,zlowbound,zhighbound);
  TH3F *pGen_3D = new TH3F("pGen","",nBinsxy,xylowbound,xyhighbound,nBinsxy,xylowbound,xyhighbound,nBinsZ,zlowbound,zhighbound);
  TH3D *LCEhist_3D = new TH3D("LCEhist","",nBinsxy,xylowbound,xyhighbound,nBinsxy,xylowbound,xyhighbound,nBinsZ,zlowbound,zhighbound);

  TH2F *pDet_RZ = new TH2F("pDet_RZ","",nBinsxy,0.,xyhighbound,nBinsZ,zlowbound,zhighbound);
  TH2F *pGen_RZ = new TH2F("pGen_RZ","",nBinsxy,0.,xyhighbound,nBinsZ,zlowbound,zhighbound);
  TH2D *LCEhist_RZ = new TH2D("LCEhist_RZ","",nBinsxy,0.,xyhighbound,nBinsZ,zlowbound,zhighbound);

  //--------------------------------------
  //Loop through # events
  //-------------------------------------
  
  cout << nEntries << " Events in the file " << endl;
  for(int n=0; n<nEntries; ++n){
    DataTree->GetEntry(n);

    vector<volumeInfo> &volumes = evt->volumes;
    vector<primaryParticleInfo> &primaryparticles = evt->primaryParticles;
    vector<trackInfo> &tracks = evt->tracks;

    float pos_x_cm, pos_y_cm, pos_z_cm, pos_r_cm;

    pos_x_cm = primaryparticles[0].dPosition_mm[0] * 0.1;
    pos_y_cm = primaryparticles[0].dPosition_mm[1] * 0.1;
    pos_z_cm = primaryparticles[0].dPosition_mm[2] * 0.1;

    pos_r_cm = pow( pow(pos_x_cm,2) + pow(pos_y_cm,2) , 0.5);

    // Fill each each event into the pGen hist
    
    pGen_3D->Fill(pos_x_cm,pos_y_cm,pos_z_cm);
    pGen_RZ->Fill(pos_r_cm,pos_z_cm);

    for (int j = 0; j < volumes.size(); j++ ){
      if (volumes[j].sName.find("PMT") != std::string::npos ){

	// If photon hits PMT fill pDet hist
	//cout << volumes[j].sName << endl;
	pDet_3D->Fill(pos_x_cm,pos_y_cm,pos_z_cm);
	pDet_RZ->Fill(pos_r_cm,pos_z_cm);

	break;
      }// end loop over events that hit PMT
    } // end loop over volumes

    
  } //End event loop

  

  //Create and open file to fill with X,Y,Z,LCE
  ofstream file, file1;
  //file.open("Skin_LXe_LCE_3D_MDC3_BACC528_274mInputs_2cmRes.txt");
  //file1.open("Skin_LXe_LCE_2D_MDC3_BACC528_274mInputs_1cmRes.txt");

  cout << "Made it to the loop" << endl;

  // Fill LCEhist in R,Z space
  for (int i = 1; i < pGen_RZ->GetNbinsX()+1; i++) {
    for (int j = 1; j < pGen_RZ->GetNbinsY()+1; j++) {

      double numPGen_RZ = pGen_RZ->GetBinContent(i,j);
      double numPDet_RZ = pDet_RZ->GetBinContent(i,j);
      
      double Pratio_RZ = 0;
      if (numPGen_RZ != 0){Pratio_RZ = numPDet_RZ/numPGen_RZ;}

      // Smooth a bit
      if (numPGen_RZ != 0 && numPGen_RZ == 0){
	Pratio_RZ = ((pDet_RZ->GetBinContent(i-1,j)+pDet_RZ->GetBinContent(i+1,j))/2) / ((pGen_RZ->GetBinContent(i-1,j)+pGen_RZ->GetBinContent(i+1,j))/2);
      }

      LCEhist_RZ->SetBinContent(i,j,Pratio_RZ);
      
      double Rcoord_RZ = pGen_RZ->GetXaxis()->GetBinCenter(i);
      double Zcoord_RZ = pGen_RZ->GetYaxis()->GetBinCenter(j);
      
      //file1 << Rcoord_RZ << ", " << Zcoord_RZ << ", "<<  Pratio_RZ << "\n";

    }
  }
  /*
  // Now we will fill the LCEhist in X,Y,Z
  for (int i = 1; i < pGen_3D->GetNbinsX()+1; i++) { 
    for (int j = 1; j < pGen_3D->GetNbinsY()+1; j++) { 
        for (int k = 1; k < pGen_3D->GetNbinsZ()+1; k++) { 
		
	double numPGen = pGen_3D->GetBinContent(i,j,k);
	double numPDet = pDet_3D->GetBinContent(i,j,k);

	double Xcoord = pGen_3D->GetXaxis()->GetBinCenter(i);
	double Ycoord = pGen_3D->GetYaxis()->GetBinCenter(j);
	double Zcoord = pGen_3D->GetZaxis()->GetBinCenter(k);

	double Pratio = 0;
	if (numPGen != 0){Pratio = numPDet/numPGen;}

	LCEhist_3D->SetBinContent(i,j,k,Pratio); //Fill the LCE Hist
	file << Xcoord << ", " << Ycoord << ", " << Zcoord << ", "<<  Pratio << "\n"; //write the text file for QHull use
	
      }
    }
  } //done with that loop
  */

  //file.close();
  //file1.close();
  
  ////////////////////////////
  // Plot the  histograms                                                                                                                                                        

  TCanvas * c1 = new TCanvas();
  //LCEhist_RZ->SetMaximum(0.35);
  //LCEhist_RZ->SetStats(0);
  LCEhist_RZ->GetXaxis()->CenterTitle(true);
  LCEhist_RZ->GetXaxis()->SetTitle("R [cm]");
  LCEhist_RZ->GetYaxis()->CenterTitle(true);
  LCEhist_RZ->GetYaxis()->SetTitle("Z [cm]");
  LCEhist_RZ->Draw("COLZ");
  

}
