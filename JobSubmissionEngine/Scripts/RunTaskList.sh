#!/bin/bash

let LineNumber=$(expr ${SLURM_ARRAY_TASK_ID}\*${SLURM_NTASKS}+${SLURM_LOCALID}+1)

Line=`sed "${LineNumber}q;d" $1`

if [ ! -z "$Line" ]; then
    eval $Line
fi

