import glob
import os,sys
import math as m
import subprocess
import socket

class JobFactory:

    #--------------------------------------------
    # Constructor
    #--------------------------------------------
    def __init__(self, jobName, account, image='image_lz_centos7', cluster='default', queue='regular', time='00:08:00', corePerTask=1, useHyperThreading=True, reservation=None):

        self.jobName = jobName

        self.task_list = []
        self.jobs_list = []
        
        #--------------------------------------------
        # List of the of the supported images
        #--------------------------------------------
        imagesList = {}
        imagesList['image_pdsf'] = 'custom:pdsf-chos-sl64:v5'
        imagesList['image_lz_centos7'] = 'luxzeplin/offline_hosted:centos7'
        imagesList['image_lz_centos7_1'] = 'luxzeplin/offline_hosted:centos7_1'
        
        #--------------------------------------------
        # Check the image tag 
        #--------------------------------------------
        if image not in imagesList:
            print('Impossible to find the image:', image)
            print('Possible options are:\n',imagesList.keys())
            exit(1)
        self.shifterImage = imagesList[image]

        #--------------------------------------------
        # Detect the current cluster
        #--------------------------------------------
        if cluster == 'default':
            hostname = socket.gethostname()

            if hostname.find('cori') != -1 :
                print('WARNING: You are rining on CORI, the defaul CORI cluster is haswell')
                self.cluster='haswell'
            else: 
                self.cluster='interactive'

        elif cluster != 'interactive' and cluster != 'knl' and cluster != 'haswell':
            print('Impossible to find the runing mode.')
            print('Possible options are: \'interactive\',  \'knl\' or \'haswell\'')
            exit(1)
        else:
            self.cluster=cluster
        
        #--------------------------------------------
        # Number of cores available for each cluster
        #--------------------------------------------
        self.defCorePerNode = {'knl': 68, 'haswell': 32, 'interactive':1}
        self.defHyperTreadingFactor = {'knl': 4, 'haswell': 2, 'interactive':1}

        self.defThreadsPerNode = self.defCorePerNode[self.cluster]
        if useHyperThreading: 
            self.defThreadsPerNode *= self.defHyperTreadingFactor[self.cluster]
        

        

        self.account = account
        self.queue = queue
        self.time = time
        self.corePerTask = corePerTask
        self.reservation = reservation

    #--------------------------------------------
    #--------------------------------------------

    #--------------------------------------------
    # Add a new job to the task list
    #--------------------------------------------
    def AddNewJob(self, jobCommand):
        self.task_list.append(jobCommand)
    #--------------------------------------------
    #--------------------------------------------

    #--------------------------------------------
    #
    #--------------------------------------------
    def BuildJobs(self):

        os.system('mkdir -p Jobs')

        if self.cluster == 'knl' or self.cluster == 'haswell':
            self.BuildTaskFile(False)
            self.BuildJobsArray()
        elif self.cluster == 'interactive':
            self.BuildTaskFile(True)
        else:
            print("Unknown mode:", mode)
            exit(1)
    #--------------------------------------------
    #--------------------------------------------

    #--------------------------------------------
    #--------------------------------------------
    def BuildTaskFile(self, isInteractive):

        self.taskListFileName = 'Jobs/task_{0}_{1}.sh'.format(self.cluster, self.jobName)

        task_file = open(self.taskListFileName,'w')

        for i,task in enumerate(self.task_list):
            if self.cluster == 'knl' or self.cluster == 'haswell':
                if isInteractive:
                    task_file.write('shifter --image={0} /bin/bash -c \'{1}\'\n'.format(self.shifterImage,task))
                else:
                    task_file.write('shifter /bin/bash -c \'{0}\'\n'.format(task))
            else:
                task_file.write('{0}\n'.format(task))

        task_file.close()
        os.system('chmod u+x {0}'.format(self.taskListFileName))
        
        
    #--------------------------------------------
    #--------------------------------------------
    def BuildJobsArray(self):

        nNode = 1

        nCorePerNode = self.defThreadsPerNode
        
        nTaskPerNode = int(m.floor(nCorePerNode / float(self.corePerTask)))
        if nTaskPerNode ==0: nTaskPerNode = 1

        nTasks = len(self.task_list)
        if nTasks < nTaskPerNode: nTaskPerNode = nTasks

        nArray = int(m.ceil(nTasks / float(nTaskPerNode))) - 1

        slrm_file_name = 'Jobs/slurm_{0}_{1}.sh'.format(self.cluster, self.jobName)

        slrm_file = open(slrm_file_name, 'w')

        slrm_file.write('#!/bin/bash\n')

        if self.cluster == 'knl' or self.cluster == 'haswell':
            slurm_header = self.GetCoriSlurmHeader(1)
        
            
        slrm_file.write(slurm_header + '\n')
        
        slrm_file.write('#SBATCH --account={0}\n'.format(self.account))

        if self.reservation != None:
            slrm_file.write('#SBATCH --reservation={0}\n'.format(self.reservation))
        #--------------------------------------------
        ## Setup the logs output
        #--------------------------------------------
        log_path = '{0}/job_output/{1}'.format(os.path.abspath('.'), self.jobName)

        os.system('mkdir -p {0}'.format(log_path))

        slrm_file.write('#SBATCH --output={0}/{1}.%A_%a.out\n'.format(log_path, self.jobName))
        slrm_file.write('#SBATCH --error={0}/{1}.%A_%a.err\n'.format(log_path, self.jobName))

        slrm_file.write('#SBATCH --module=cvmfs\n')
        slrm_file.write('#SBATCH --image={0}\n'.format(self.shifterImage))

        slrm_file.write('#SBATCH --array=0-{0}\n'.format(nArray))
        
        slrm_file.write('''\n# OpenMP settings:
export OMP_NUM_THREADS={0}
export OMP_PLACES=threads
export OMP_PROC_BIND=spread\n\n\n'''.format(self.corePerTask))

        dirname, filename = os.path.split(os.path.abspath(__file__))
        
        cpu_bin='cores'
        if self.defThreadsPerNode > self.defCorePerNode[self.cluster]: 
            cpu_bin='threads'

        slrm_file.write('srun --no-kill --kill-on-bad-exit=0 --cpu-bind=threads -n {0} -c {1} {2}/Scripts/RunTaskList.sh {3}\n\n'.format(nTaskPerNode, self.corePerTask, dirname, self.taskListFileName))

        slrm_file.close()

        self.jobs_list.append(slrm_file_name)

    #--------------------------------------------
    #--------------------------------------------


    #--------------------------------------------
    #--------------------------------------------
    def GetCoriSlurmHeader(self, nNode):
        slurm_header = '''#################
#SBATCH -N {0} # number of nodes
#SBATCH -C {1} # hardware architecture
#SBATCH --time={2}
#SBATCH --qos={3}
#SBATCH --job-name={4}
#SBATCH --license=project,projecta
#SBATCH -L SCRATCH
#################

'''
        slurm_header = slurm_header.format(nNode, self.cluster, self.time, self.queue, self.jobName)
        return slurm_header
    #--------------------------------------------
    #--------------------------------------------

    #--------------------------------------------
    #--------------------------------------------
    def RunJobs(self):

        print("-----------------------------------")
        print("Execute the jobs:")

        if self.cluster == 'knl' or self.cluster == 'haswell':
            print('No automatic job submission.')
            print('To submit the jobs to the queue, run the following commands:')
            for job in self.jobs_list:
                print('sbatch {0}'.format(job))

        elif self.cluster == 'interactive':

            cmd = './{0}'.format(self.taskListFileName)
            print(cmd)

        else:
            print("Unknown mode:", self.cluster)
            exit(1)
    #--------------------------------------------
    #--------------------------------------------
