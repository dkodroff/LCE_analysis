# Job submission engine

This repo contains the CORI job submission engine. 
This python code helps the LZ users to submit jobs at the USDC.
You can add this repo to your projects by using the submodule feature.

## JobFactory usage

The `JobFactory` is a python class defined in the file `JobFactory.py`.
The complete prototype of the class constructor is:  
```python
 JobFactory(jobName, account, image, cluster, queue, time, corePerTask, useHyperThreading)
```

Arguments: 

- jobName [string]: Name of the job
- account [string]: account name (aka LZ)
- image [string]: Tag name of the image. Supported tags: `image_pdsf` or `image_lz_centos7`. Defaul: `image_lz_centos7`
- cluster [string]: Name of the cluster: `haswell`, `knl` or `interactive`. Default: `haswell`
- queue [string]: Name of the queue. See https://docs.nersc.gov/jobs/policy/ for more details. Default `regular`
- time [string]: Job execution time in format: `HH:MM:SS`. Default: `00:08:00`
- corePerTask [int]: Number of core per tash. This variable can be used to increase the memory allocation per task. Default: `1`
- useHyperThreading [bool]: Turn On/Off the hyper-threading. Default: `True`


## Use case

The script `UsageExample.py` shows a simple use case of the `JobFactory`

```python 
import sys

from JobFactory import JobFactory

factory = JobFactory('dummy', 'lz')

for i in range(10):

    command = 'echo "Job number {0}"'.format(i)
    command += ' && ls'

    factory.AddNewJob(command)

factory.BuildJobs()
factory.RunJobs()
```

## Add to your projects


You can add this repository by using the git feature `submodule`. See the [full documentation](https://git-scm.com/book/en/v2/Git-Tools-Submodules).


To add it to your repo, go inside you folder and execute the command: 
```
git submodule add git@gitlab.com:luxzeplin/lz_services/JobSubmissionEngine.git
```

This will add the submodule to you repository. 
To commit the submodule addition, in the same directory, do 
```
git commit -a -m "Add JobSubmissionEngine submodule"

```

Be carefull with the option `-a` if you have some un-commited work.



When you clone you project, you have 2 ways:

1) Clone the repo only
   ```
   git clone /url/to/your/repo
   ```
   and init the submodules with: 
   ```
   git submodule init
   git submodule update
   ```

2) Clone the repo and all the submodules
   ```
   git clone --recurse-submodules /url/to/your/repo
   ```
   	 
