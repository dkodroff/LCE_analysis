import os,sys
from JobFactory import JobFactory

job_factory = JobFactory('TPCPhotonSims','lz',image='image_lz_centos7',cluster='haswell',queue='regular',time='2:00:00',corePerTask=1)

seed = 12000

skin_macro = '/global/homes/d/dsk192/LCE_amalysis/photonbomb_TPC.mac'
bacc_exec = '/cvmfs/lz.opensciencegrid.org/BACCARAT/release-5.2.8/x86_64-centos7-gcc7-opt/bin/BACCARATExecutable'
setup_sh = '/cvmfs/lz.opensciencegrid.org/BACCARAT/release-5.2.8/x86_64-centos7-gcc7-opt/setup.sh'


for i in range(200):

    command = 'source {0}'.format(setup_sh)
    command +=' && export SEED="{0}"'.format(seed)
    command += ' && {0} {1}'.format(bacc_exec, skin_macro)

    job_factory.AddNewJob(command)

    seed += 1


job_factory.BuildJobs()
job_factory.RunJobs()
