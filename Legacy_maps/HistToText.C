#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH1D.h"
#include "THStack.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH3F.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMinuit.h"
#include "TColor.h"
#include "TLine.h"
#include "TLatex.h"
#include "TSystem.h"

#include <stdio.h>
#include <iostream>
#include <TH2D.h>
#include <TH3D.h>
#include <TAxis.h>
#include <TFile.h>
#include <TStyle.h>
#include <TTree.h>
#include <TROOT.h>
using namespace std;


void HistToText(){


  TFile* infile = TFile::Open("PDEandEField-Sep-18-17.root");
  TH3D *pdeMap = (TH3D*)infile->Get("pdeMap");


  ofstream file1;
  file1.open("Skin_pdeSmoothed_3Dhist_TDR-Sep-18-17.txt");

  int nX = pdeMap->GetNbinsX(); 
  int nY = pdeMap->GetNbinsY();
  int nZ = pdeMap->GetNbinsZ();

  double xMin = pdeMap->GetXaxis()->GetXmin();
  double xMax = pdeMap->GetXaxis()->GetXmax();
  double yMin = pdeMap->GetYaxis()->GetXmin();
  double yMax = pdeMap->GetYaxis()->GetXmax();
  double zMin = pdeMap->GetZaxis()->GetXmin();
  double zMax = pdeMap->GetZaxis()->GetXmax();

  file1 << nX << "\t" << xMin << "\t" << xMax << "\t" << nY << "\t" << yMin << "\t" << yMax << "\t" << nZ << "\t" << zMin << "\t" << zMax << "\n";

  for (int i = 1; i <=nX; i++) {
    for (int j = 1; j <=nY; j++) {
      for (int k = 1; k <=nZ; k++) {

	double pde = pdeMap->GetBinContent(i,j,k);
	double lce_avg = pde / 0.24; // QE of skin PMTs is 0.19 for top and 0.3 for bottom
	int bin = pdeMap->GetBin(i,j,k);
	
	if (lce_avg != 0 ){
	  file1 << bin << "\t" << lce_avg << "\n";
	}

      }
    }
  }

  infile->Close();
  file1.close();

}
